#!/bin/python3
import socket
import threading
import time
import sys
import os
import signal
import errno
import Adafruit_BBIO.ADC as ADC  # Library to use ADC pins
import Adafruit_BBIO.GPIO as GPIO # Library to use GPIO pins

dP0="P8_8"
dP1="P8_9"
dP2="P8_10"
dP3="P8_11"
dP4="P8_12"
dP5="P8_14"
dP6="P8_15"
dP7="P8_16"
dP8="P8_17"
dP9="P8_18"

ADC.setup()
aP0="P9_39"
aP1="P9_40"
aP2="P9_37"
aP3="P9_38"
aP4="P9_33"
aP5="P9_36"
aP6="P9_35"

for r in (dP0,dP1,dP2,dP3,dP4,dP5,dP6,dP7,dP8,dP9):
  GPIO.setup(r,GPIO.OUT)

ton=open("VON.txt","r")
VON=ton.read(6)
VON=float(VON)
ton.close()

toff=open("VOFF.txt","r")
VOFF=toff.read(6)
VOFF=float(VOFF)
toff.close()

clients=[]
address=[]
serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
serv.bind(('0.0.0.0', 8084))
serv.listen()

AUT0=1
AUT1=1
AUT2=1
AUT3=1
DOUT0=0
DOUT1=0
DOUT2=0
DOUT3=0
AUTM=0

try:

   def conne():
         global conn,addr
         global le
         le=0
         while True:
             conn,addr=serv.accept()
             clients.append(conn)
             address.append(addr)
             print("Accepted new connection from:", addr)
             if le<len(clients):
                r=threading.Thread(target =rec, args=(le,))
                s=threading.Thread(target =send, args=(le,))
                le=le+1
                s.start()
                r.start()
   def send(i):
       global le
       global DOUT0
       while True:
           try:
                p0=("AIN"+'|0 '+str(pv0)+'|'+'1 '+str(pv1)+'|'+'2 '+str(pv2)+'|'+'3 '+str(pv3)+'|'+'4 '+str(pv4)+'|'+'5 '+str(pv5)+'|'+'6 '+str(pv6)+"\n") #Takes the values and 
                p1=("DOUT|0 "+str(DOUT0)+ "|1 "+str(DOUT1)+"|2 "+str(DOUT2)+"|3 "+str(DOUT3)+"\n")
                msg=p0+p1
                clients[i].send(bytes(msg,"utf-8"))
                time.sleep(0.5)
           except IOError as e:
               if e.errno == errno.EPIPE:
                  print("Connection Lost from:", address[i])
                  clients.pop(i)
                  address.pop(i)
                  le=le-1
           except IndexError:
              sys.exit(0)

   def rec(i):
       global AUT0
       global AUT1
       global AUT2
       global AUT3
       global DOUT0
       global DOUT1
       global DOUT2
       global DOUT3
       while True:
          try:
             data=clients[i].recv(4096)
             if not data: continue
             d=data.decode("utf-8")
             st=str(d)
             print(st)
             if st=="AUTM\n":
                AUT0=1
                AUT1=1
                AUT2=1
                AUT3=1

             if st=="AUT0\n":
                AUT0=1

             if st=="DOUT0|ON\n":
                DOUT0=1
                AUT0=0
                AUTM=0

             if st=="DOUT0|OFF\n":
                DOUT0=0
                AUT0=0
                AUTM=0

             if st=="AUT1\n":
                AUT1=1

             if st=="DOUT1|ON\n":
                DOUT1=1
                AUT1=0
                AUTM=0

             if st=="DOUT1|OFF\n":
                DOUT1=0
                AUT1=0
                AUTM=0

             if st=="AUT2\n":
                AUT2=1

             if st=="DOUT2|ON\n":
                DOUT2=1
                AUT2=0
                AUTM=0

             if st=="DOUT2|OFF\n":
                DOUT2=0
                AUT2=0
                AUTM=0

             if st=="AUT3\n":
                AUT3=1

             if st=="DOUT3|ON\n":
                DOUT3=1
                AUT3=0
                AUTM=0

             if st=="DOUT3|OFF\n":
                DOUT3=0
                AUT3=0
                AUTM=0
          except IndexError:
              sys.exit(0)

   def OUT(AUT,DOUT,dP,pv):
       if AUT==1:
          if DOUT==0:
             if pv>=VON:
                GPIO.output(dP, GPIO.HIGH)
                DOUT=1
             else:
                GPIO.output(dP,GPIO.LOW)
          if DOUT==1:
             if pv<=VOFF:
                GPIO.output(dP, GPIO.LOW)
                DOUT=0
             else:
                GPIO.output(dP,GPIO.HIGH)
       if AUT==0:
          if DOUT==0:
             GPIO.output(dP, GPIO.LOW)
          if DOUT==1:
             GPIO.output(dP, GPIO.HIGH)
       return DOUT
   c=threading.Thread(target =conne)
   c.start()
   while True:
       pv0=round((ADC.read(aP0)*1.8),3)
       pv1=round((ADC.read(aP1)*1.8),3)
       pv2=round((ADC.read(aP2)*1.8),3)
       pv3=round((ADC.read(aP3)*1.8),3)
       pv4=round((ADC.read(aP4)*1.8),3)
       pv5=round((ADC.read(aP5)*1.8),3)
       pv6=round((ADC.read(aP6)*1.8),3)
       p0=OUT(AUT0,DOUT0,dP0,pv0)
       if p0==0:
          DOUT0=0
       else:
          DOUT0=1
       p1=OUT(AUT1,DOUT1,dP1,pv1)
       if p1==0:
          DOUT1=0
       else:
          DOUT1=1
       p2=OUT(AUT2,DOUT2,dP2,pv2)
       if p2==0:
          DOUT2=0
       else:
          DOUT2=1
       p3=OUT(AUT3,DOUT3,dP3,pv3)
       if p3==0:
          DOUT3=0
       else:
          DOUT3=1
       time.sleep(0.5)

except KeyboardInterrupt:
   conn.close()
   serv.close()
   print(" ")
   os._exit(1)


