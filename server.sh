#!/bin/bash

cd /home/debian/monit

case $1 in
start)
exec 2>&1 python3 serv.py >/dev/null &
echo $! > pid;
;;
stop)
kill -9 $(cat pid) ;;
*)
echo "usage: ./server.sh {start|stop}" ;;
esac

exit 0
