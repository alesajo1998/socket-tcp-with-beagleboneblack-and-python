$(function(){
    $("#m1").dxCircularGauge({
        scale: {
            startValue: 0, endValue: 100,
            tick: {

                color: "#000000"
            },

            tickInterval: 10,

            label: {
                indentFromTick: 7,
                customizeText: function (arg) {
                    return arg.valueText + "A";
                }

            }
        },
        rangeContainer: {
            offset: -5,
            ranges: [
                { startValue: 0, endValue: 30, color: "#77DD77" },
                { startValue: 30, endValue: 70, color: "#E6E200" },
                { startValue: 70, endValue: 100, color: "#FF0000" }
            ]
        },

        valueIndicator: {
            offset: 10,
            color: "#000000"
        },
        subvalueIndicator: {
            offset: -10,
            type: "textCloud",
            color: "#00000080"

        },
        title: {
            text: "AIN0",
            font: { size: 20 }
            
        },

        value: 3.4,
        subvalues: [3.4]

    });
    $("#m2").dxCircularGauge({
        scale: {
            startValue: 0, endValue: 100,
            tick: {

                color: "#000000"
            },

            tickInterval: 10,

            label: {
                indentFromTick: 7,
                customizeText: function (arg) {
                    return arg.valueText + "A";
                }

            }
        },
        rangeContainer: {
            offset: -5,
            ranges: [
                { startValue: 0, endValue: 30, color: "#77DD77" },
                { startValue: 30, endValue: 70, color: "#E6E200" },
                { startValue: 70, endValue: 100, color: "#FF0000" }
            ]
        },

        valueIndicator: {
            offset: 10,
            color: "#000000"
        },
        subvalueIndicator: {
            offset: -10,
            type: "textCloud",
            color: "#00000080"

        },
        title: {
            text: "AIN1",
            font: { size: 20 }
            
        },

        value: 1,
        subvalues: [1]

    });
    $("#m3").dxCircularGauge({
        scale: {
            startValue: 0, endValue: 100,
            tick: {

                color: "#000000"
            },

            tickInterval: 10,

            label: {
                indentFromTick: 7,
                customizeText: function (arg) {
                    return arg.valueText + "A";
                }

            }
        },
        rangeContainer: {
            offset: -5,
            ranges: [
                { startValue: 0, endValue: 30, color: "#77DD77" },
                { startValue: 30, endValue: 70, color: "#E6E200" },
                { startValue: 70, endValue: 100, color: "#FF0000" }
            ]
        },

        valueIndicator: {
            offset: 10,
            color: "#000000"
        },
        subvalueIndicator: {
            offset: -10,
            type: "textCloud",
            color: "#00000080"

        },
        title: {
            text: "AIN2",
            font: { size: 20 }
            
        },

        value: 45,
        subvalues: [45]

    });
    $("#m4").dxCircularGauge({
         scale: {
            startValue: 0, endValue: 100,
            tick: {

                color: "#000000"
            },

            tickInterval: 10,

            label: {
                indentFromTick: 7,
                customizeText: function (arg) {
                    return arg.valueText + "A";
                }

            }
        },
        rangeContainer: {
            offset: -5,
            ranges: [
                { startValue: 0, endValue: 30, color: "#77DD77" },
                { startValue: 30, endValue: 70, color: "#E6E200" },
                { startValue: 70, endValue: 100, color: "#FF0000" }
            ]
        },

        valueIndicator: {
            offset: 10,
            color: "#000000"
        },
        subvalueIndicator: {
            offset: -10,
            type: "textCloud",
            color: "#00000080"

        },
        title: {
            text: "AIN3",
            font: { size: 20 }
            
        },

        value: 100,
        subvalues: [100]

    });
    $("#m5").dxCircularGauge({
        scale: {
            startValue: 0, endValue: 100,
            tick: {

                color: "#000000"
            },

            tickInterval: 10,

            label: {
                indentFromTick: 7,
                customizeText: function (arg) {
                    return arg.valueText + "A";
                }

            }
        },
        rangeContainer: {
            offset: -5,
            ranges: [
                { startValue: 0, endValue: 30, color: "#77DD77" },
                { startValue: 30, endValue: 70, color: "#E6E200" },
                { startValue: 70, endValue: 100, color: "#FF0000" }
            ]
        },

        valueIndicator: {
            offset: 10,
            color: "#000000"
        },
        subvalueIndicator: {
            offset: -10,
            type: "textCloud",
            color: "#00000080"

        },
        title: {
            text: "AIN4",
            font: { size: 20 }
            
        },

        value: 150,
        subvalues: [150]

    });
    $("#m6").dxCircularGauge({

        scale: {
            startValue: 0, endValue: 100,
            tick: {

                color: "#000000"
            },

            tickInterval: 10,

            label: {
                indentFromTick: 7,
                customizeText: function (arg) {
                    return arg.valueText + "A";
                }

            }
        },
        rangeContainer: {
            offset: -5,
            ranges: [
                { startValue: 0, endValue: 30, color: "#77DD77" },
                { startValue: 30, endValue: 70, color: "#E6E200" },
                { startValue: 70, endValue: 100, color: "#FF0000" }
            ]
        },

        valueIndicator: {
            offset: 10,
            color: "#000000"
        },
        subvalueIndicator: {
            offset: -10,
            type: "textCloud",
            color: "#00000080"

        },
        title: {
            text: "AIN5",
            font: { size: 20 }
            
        },

        value: 30,
        subvalues: [30]

    });
    $("#m7").dxCircularGauge({
        scale: {
            startValue: 0, endValue: 100,
            tick: {

                color: "#000000"
            },

            tickInterval: 10,

            label: {
                indentFromTick: 7,
                customizeText: function (arg) {
                    return arg.valueText + "A";
                }

            }
        },
        rangeContainer: {
            offset: -5,
            ranges: [
                { startValue: 0, endValue: 30, color: "#77DD77" },
                { startValue: 30, endValue: 70, color: "#E6E200" },
                { startValue: 70, endValue: 100, color: "#FF0000" }
            ]
        },

        valueIndicator: {
            offset: 10,
            color: "#000000"
        },
        subvalueIndicator: {
            offset: -10,
            type: "textCloud",
            color: "#00000080"

        },
        title: {
            text: "AIN6",
            font: { size: 20 }
            
        },

        value: 65,
        subvalues: [65]

    });
});