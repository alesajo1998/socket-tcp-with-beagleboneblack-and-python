#Explanation
#First we have to import the libraries that we need. 

import socket #Library to create socket TCP  
import threading #Library to be able to make two or more processes at the same time.  
import time #Library to control the time
import sys #Library to contol the system 
import os #Operative system
import signal # Set handlers for asynchronous events
import errno #Library to control the exceptions
import Adafruit_BBIO.ADC as ADC  # Library to use ADC pins
import Adafruit_BBIO.GPIO as GPIO # Library to use GPIO pins


#Digital pin's names. 
dP0="P8_8"
dP1="P8_9"
dP2="P8_10"
dP3="P8_11"
dP4="P8_12"
dP5="P8_14"
dP6="P8_15"
dP7="P8_16"
dP8="P8_17"
dP9="P8_18"

#Analog pin's names. 

ADC.setup() # This is the setup for analogs pins
aP0="P9_39"
aP1="P9_40"
aP2="P9_37"
aP3="P9_38"
aP4="P9_33"
aP5="P9_36"
aP6="P9_35"

#The setup por digital PINS. 
for r in (dP0,dP1,dP2,dP3,dP4,dP5,dP6,dP7,dP8,dP9):
  GPIO.setup(r,GPIO.OUT)

#Openes the the text which contains the voltage that, in AUT mode, is going to turn on the digital PIN
ton=open("VON.txt","r") #Name of the archive  with the read feature.
VON=ton.read(6)# From str to float
ton.close()#Close the text

#Openes the the text which contains the voltage that, in AUT mode, is going to turn OFF the digital PIN
toff=open("VOFF.txt","r") #Name of the archive  with the read feature.
VOFF=toff.read(6)# From str to float
toff.close()#Close the text

clients=[] #List of clients
address=[] #List of addresses 
serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)# This is the setup for the server (SOCKET TCP)
serv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)#With this command it clears the port and avoids to have connections problems
serv.bind(('0.0.0.0', 8084)) #It is going to listen to any address and its port is 8084
serv.listen() 

#First parameters 
AUT0=1
AUT1=1
AUT2=1
AUT3=1
DOUT0=0
DOUT1=0
DOUT2=0
DOUT3=0
AUTM=0

try: #For exceptions (keboard interrupt) 


#This is the first thread. 
   def conne():
         global conn,addr #Make global the variables conn and addr
         global le #Make global the variable le
         le=0 #Firt parameter
         while True:
             conn,addr=serv.accept() #When there is a client trying to connect it is going to accept the connection and also it is going to save the client's information in the conn,addr variable 
             clients.append(conn) #It adds the conn information to the list of clients. 
             address.append(addr) #It adds the address information to the address list. 
             print("Accepted new connection from:", addr)# It displays the text and the address of the client that just connected. 
             if le<len(clients): #If variable le is less than the lenght of clients 
                r=threading.Thread(target =rec, args=(le,)) # It will start 2 threads. The first is the receive thread that listens to client. The second one is the send thread that sends the information to the client.  
                s=threading.Thread(target =send, args=(le,))# The arg is going to be the number of clients connected.
                le=le+1 #When this happenes it will add 1 to the variable to make them equal. 
                s.start() # Start thread
                r.start() #Start thread

# The send thread mentioned before
   def send(i):
       global le #Make global le because it is going to be modified in this thread
       while True:
           try:
               #These are the messages that it is going to send 
               #In P0 it has the analog values taken in the ADC pins and organized as it is shown.
                p0=("AIN"+'|0 '+str(pv0)+'|'+'1 '+str(pv1)+'|'+'2 '+str(pv2)+'|'+'3 '+str(pv3)$
               #In P1 it has the digital pins and shows if they are on or off with a binary number. 
                p1=("DOUT|0 "+str(DOUT0)+ "|1 "+str(DOUT1)+"|2 "+str(DOUT2)+"|3 "+str(DOUT3)+"$
                #The whole message
                msg=p0+p1
                # Sends the information to the client. 
                clients[i].send(bytes(msg,"utf-8"))
                #Delay of 0.5 seg
                time.sleep(0.5)
           except IOError as e: #Exception
               if e.errno == errno.EPIPE: #If client is not longer available. 
                  print("Connection Lost from:", address[i])#It is going to print which client disconected
                  clients.pop(i) #It is going to remove the client from the clients list
                  address.pop(i) #It is going to remove the address from the address list
                  le=le-1 #It is going to substract 1 to the variable to make it equal with the lenght of the client list.
           except IndexError: #This error is going to happen after it removes the client from the list. 
              sys.exit(0) # It is going to close the thread. 

#The receive thread mentioned before.
def rec(i):
      #Make global the next variables
       global AUT0 #Automatic mode for digitalPin 0 depending on the values of the analogPin and de VON y VOFF texts  and so on...
       global AUT1
       global AUT2
       global AUT3
       global DOUT0 #The value of the digitalPin 0. If it is 1 it is going to be ON if it is 0 it is going to be OFF and so on...
       global DOUT1
       global DOUT2
       global DOUT3
       while True:
          try:
             data=clients[i].recv(4096) #Listens to the client
             if not data: continue #If there is not message continue
             d=data.decode("utf-8") #Decode the information that arrives. 
             st=str(d)#Convert it to string
             print(st)#Print the message.
             if st=="AUTM\n": #If it receives this commnad all the pins are going to be in AUT mode.  
                AUT0=1
                AUT1=1
                AUT2=1
                AUT3=1

             if st=="AUT0\n": #If it receives this command only the Pin0 is going to be in AUT mode
                AUT0=1

             if st=="DOUT0|ON\n": #If it receives this command the digitalPin0 is going to turn ON an it will be in MANUAL mode. 
                DOUT0=1
                AUT0=0
                AUTM=0

             if st=="DOUT0|OFF\n": #If it receives this command the digitalPin0 is going to turn OFF an it will be in MANUAL mode.
                DOUT0=0
                AUT0=0
                AUTM=0

            #The same happens with the other pins. 

             if st=="AUT1\n":
                AUT1=1

             if st=="DOUT1|ON\n":
                DOUT1=1
                AUT1=0
                AUTM=0

             if st=="DOUT1|OFF\n":
                DOUT1=0
                AUT1=0
                AUTM=0

             if st=="AUT2\n":
                AUT2=1

             if st=="DOUT2|ON\n":
                DOUT2=1
                AUT2=0
                AUTM=0

             if st=="DOUT2|OFF\n":
                DOUT2=0
                AUT2=0
                AUTM=0

             if st=="AUT3\n":
                AUT3=1

             if st=="DOUT3|ON\n":
                DOUT3=1
                AUT3=0
                AUTM=0

             if st=="DOUT3|OFF\n":
                DOUT3=0
                AUT3=0
                AUTM=0
          except IndexError:#When the connection closes it finishes the thread.
              sys.exit(0)

#This is a funtion that is used in the MAIN loop below. This is in charge of making the decisions. It has to decide if the pin turns on or off depending on the mode or the commands showed before. 
   def OUT(AUT,DOUT,dP,pv): #THe list of parameters depend on the pin. These are set below. 
       if AUT==1: #If the pin is in AUT mode 
          if DOUT==0: #, the pin is OFF 
             if pv>=VON:# and the voltage is higher than the voltage set in the VON archive it is going to turn ON the pin and sets DOUT in 1
                GPIO.output(dP, GPIO.HIGH) #Command to turn on the pin.
                DOUT=1 
             else:
                GPIO.output(dP,GPIO.LOW) #If that doesn't happen it keeps the pin OFF. 
          if DOUT==1: #If the pin is ON 
             if pv<=VOFF: #And the voltage is under the parameter set in the VOFF archive it is going to turn OFF the pin and sets the DOUT in 0
                GPIO.output(dP, GPIO.LOW)
                DOUT=0
             else:
                GPIO.output(dP,GPIO.HIGH) #Else it shall keep the pin ON
       if AUT==0: #If aut mode is 0 
          if DOUT==0: #And the DOUT is 0 then it will turn OFF the digital Pin. 
             GPIO.output(dP, GPIO.LOW)
          if DOUT==1:# But if DOUT is 1 then it will turn ON the digital Pin. 
             GPIO.output(dP, GPIO.HIGH)
       return DOUT #This will return a 0 or a 1 depending on the value of DOUT
       
       
   c=threading.Thread(target =conne) #This will set and start the conne thread
   c.start()
   while True: # THE MAIN LOOP
       pv0=round((ADC.read(aP0)*1.8),3) #This command will read the analogPin0, multiply it by 1.8 and it will have maximum 3 decimals 
       pv1=round((ADC.read(aP1)*1.8),3) # The same for the other pins
       pv2=round((ADC.read(aP2)*1.8),3)
       pv3=round((ADC.read(aP3)*1.8),3)
       pv4=round((ADC.read(aP4)*1.8),3)
       pv5=round((ADC.read(aP5)*1.8),3)
       pv6=round((ADC.read(aP6)*1.8),3)
       p0=OUT(AUT0,DOUT0,dP0,pv0) #This starts the funtion OUT mentioned before for pin0.  
       if p0==0:  #If the returned value is 0 then it will set DOUT0 to 0
          DOUT0=0
       else:      #If the returned value is 1 then it will set DOUT1 to 1
          DOUT0=1
          
     #The same happens for the other pins. 
       p1=OUT(AUT1,DOUT1,dP1,pv1)
       if p1==0:
          DOUT1=0
       else:
          DOUT1=1
       p2=OUT(AUT2,DOUT2,dP2,pv2)
       if p2==0:
          DOUT2=0
       else:
          DOUT2=1
       p3=OUT(AUT3,DOUT3,dP3,pv3)
       if p3==0:
          DOUT3=0
       else:
          DOUT3=1

except KeyboardInterrupt: #if there is a keyboard interrupt with ctrl+c
   conn.close() #it will close the connection
   serv.close() #Close the socket TCP
   print(" ") 
   os._exit(1) #Close the program.
